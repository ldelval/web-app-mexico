<!DOCTYPE HTML>  
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>Lab 9</title>
    </head>
    <body>  

    <?php
    // define variables and set to empty values
    $nameErr = $emailErr = $genderErr = $websiteErr = "";
    $name = $email = $gender = $comment = $website = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
    $nameErr = "Name is required";
    } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and space
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and space allowed"; 
        }
    }

    if (empty($_POST["email"])) {
    $emailErr = "Email is required";
    } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
        }
    }

      if (empty($_POST["website"])) {
        $websiteErr = "Enter a number";
      } else {
        $website = test_input($_POST["website"]);
      }

      if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
      } else {
        $gender = test_input($_POST["gender"]);
      }
    }

    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
    ?>

    <h2>PHP Form Validation Example</h2>
    <p><span class="error">* required field.</span></p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
        Name: <input type="text" name="name">
        <span class="error">* <?php echo $nameErr;?></span>
        <br><br>
        E-mail: <input type="text" name="email">
        <span class="error">* <?php echo $emailErr;?></span>
        <br><br>
        Number: <input type="text" name="website">
        <span class="error">* <?php echo $websiteErr;?></span>
        <br><br>
        Gender:
        <input type="radio" name="gender" value="female">Female
        <input type="radio" name="gender" value="male">Male
        <span class="error">* <?php echo $genderErr;?></span>
        <br><br>
        <input type="submit" name="submit" value="Submit">  
    </form>

    <?php
    echo "<h2>Your Input:</h2>";
    echo 'Your name:'.' '.$name;
    echo "<br>";
    echo 'Your email:'.' '.$email;
    echo "<br>";
    echo 'The square of your number is:'.' '.$website*$website;
    echo "<br>";
    echo 'You are a:'.' '.$gender;
    ?>

        
    <h2>Questions</h2>
    <p>
        <strong>1.</strong><br><br>
        It is good practice to separate the view from the control because it forces us to understand how the page works. By separating both, we need to understand where to implement each function and it forces us to use php, and html separately.<br><br>
        
        <strong>2.</strong><br><br>
        $_REQUEST, $_FILES, $_SESSION, $_ENV, $_COOKIE...<br><br>
        
        <strong>3.</strong><br><br>
        list(): Like array(), this is not really a function, but a language construct. list() is used to assign a list of variables in one operation.<br>
        ceil():Returns the next highest integer value by rounding up value if necessary.<br>
    </p>
    </body>
</html>