<?php 

$servername = "localhost";
$username = "root";
$password = "";
$dbName = "myDB";
$tableName = "class";
$columnAssoc = array('fnameSearch'=>'firstname', 'lnameSearch'=>'lastname', 'matSearch'=>'matricular');

include('functions.php');

$conn = connect($servername, $username, $password, $dbName);

$search=$_GET['search'];
$words=select($conn, $tableName, $columnAssoc[$search]);

$response="";
$size=0;
$condition = " WHERE ";

foreach($words as $word){
    $pos=stripos(strtolower($word),strtolower($_GET['pattern']));
    if(!($pos===false)){
        $size++;
        $condition.= $columnAssoc[$search] ."='". $word ."' OR ";
    }
}
//delete the last 'OR' in the string
$condition = substr($condition, 0, -3);
printTable($conn, $tableName, $condition);

?>