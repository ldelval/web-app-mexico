$(document).ready(function(){ 
    
    $('#add').click(function(){
        $('#addForm').css('visibility', 'visible');
        $('#deleteForm').css('visibility', 'hidden');
        $('#setForm').css('visibility', 'hidden');
        $('#searchForm').css('visibility', 'hidden');
    });
    $('#delete').click(function(){
        $('#deleteForm').css('visibility', 'visible');
        $('#addForm').css('visibility', 'hidden');
        $('#setForm').css('visibility', 'hidden');
        $('#searchForm').css('visibility', 'hidden');
    });
    $('#set').click(function(){
        $('#setForm').css('visibility', 'visible');
        $('#addForm').css('visibility', 'hidden');
        $('#deleteForm').css('visibility', 'hidden');
        $('#searchForm').css('visibility', 'hidden');
    });
    $('#search').click(function(){
        $('#searchForm').css('visibility', 'visible');
        $('#addForm').css('visibility', 'hidden');
        $('#deleteForm').css('visibility', 'hidden');
        $('#setForm').css('visibility', 'hidden');
    });
    
    $("#lnameSearch").onkeyup = function(){
        //sendRequest('lnameSearch', 'resultLName');    
    };
});