<?php
$tableName = "class";
$dbName = "myDB";

include('functions.php');

$conn = connect("localhost", "root", "", $dbName);

function delete($db, $table, $id){
        $delete = "DELETE from " . $table . " WHERE id=" . $id;
        if (!$db->query($delete))
            die("Deleting n° : " . $id . " failed. (" . $stmt->errno . ") " . $stmt->error);
    }

function deleteByMat($db, $table, $mat){
    $id = searchForMat($db, $table, $mat);
    delete($db, $table, $id);
}

if(isset($_GET['mat'])){
    deleteByMat($conn, $tableName, htmlspecialchars($_GET['mat']));
    printTable($conn, $tableName);
}

?>
