<?php 

//print datas into a table
function printTable($db, $table, $cond=null){
    $select = "SELECT * FROM " . $table;
    if($cond !=null)
        $select .= $cond;
    if ($result = $db->query($select)) {
        echo '<table id="classTable"><tr><th class="first">First Name</th><th class="second">Last Name</th><th class="third">Matricular</th></tr><tr>';
        while ($row = $result->fetch_assoc()) {
            echo "<tr class='row'><td class='first'>". $row["firstname"] . "</td><td class='second'>" . $row["lastname"] . "</td><td class='third'>" . $row["matricular"] . "</td></tr>";
        }
        echo '</table>';
        $result->free();
    }
    else echo 'Error : ' .$db->error;
}

//connect to the server using the database
function connect($servername, $username, $password, $dbName){
    $conn = new mysqli($servername, $username, $password, $dbName);
    if ($conn->connect_error) 
        die("Connection failed: " . $conn->connect_error);
    return $conn;
}

//select all data of a row
function select($db, $table, $data){
    $select = "SELECT " . $data . " FROM " . $table;
    if ($result = $db->query($select)) {
        $tab = array();
        while ($row = $result->fetch_assoc()) {
            array_push($tab, $row[$data]);
        }
        $result->free();
        return $tab;
    }
    return array();
}

function searchForMat($db, $table, $mat){
    $select = "SELECT id, matricular FROM " . $table;
    if ($result = $db->query($select)) {
        while ($row = $result->fetch_assoc()) {
            if($row['matricular'] == $mat)
                return $row["id"];
        }
        $result->free();
        return 0;
    }
    else die('Selection error ' . $db->error);
}


function setFName($db, $table, $id, $fname){
    $query = "UPDATE " . $table . " SET firstname='" . $fname . "' WHERE id=" . $id;
    if(!$stmt = $db->prepare($query))
        die("error preparing : " . $stmt->error);
    if(!$stmt->execute())
        die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
}

function setLName($db, $table, $id, $lname){
    $query = "UPDATE " . $table . " SET lastname='" . $lname . "' WHERE id=" . $id;
    $stmt = $db->prepare($query);
    if(!$stmt->execute())
        die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
}

?>