<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8"/>
        <title>Lab 11 - Students list</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    
    <body>
        
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbName = "myDB";
        $tableName = "class";
          
        //connect server
        $conn = new mysqli($servername, $username, $password);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        //create database
        if (!$conn->query("CREATE database IF NOT EXISTS ". $dbName)){
            echo "Error creating database: " . $conn->error;
        }
        
        //connect the database
        if (!$conn->query("USE ". $dbName)){
            echo "Error using database: " . $conn->error;
        }
        
        //create table
        $tabCreate = "create table IF NOT EXISTS " . $tableName . "(
        id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        matricular VARCHAR(15)
        )";
        
        if (!$conn->query($tabCreate)){
            echo "Error creating table: " . $conn->error;
        }
        
        include('functions.php');
    ?>
        <header>
            <nav id="nav">
                <ul>
                    <li><a href='#' id="add">Add a student</a></li>
                    <li><a href='#' id="delete">Delete a student</a></li>
                    <li><a href='#' id="set">Set a student</a></li>
                    <li><a href="#" id="search">Search for a student</a></li>
                </ul>
            </nav>
        </header>
        
        <main>
            <section id="forms">
                <?php include('forms.html'); ?>                
            </section>
            
            <section id="tableSection">
                <?php
                printTable($conn, $tableName);
                ?>
            </section>
        </main>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
        <script src="code.js" type="text/javascript"></script>
        <script src="ajax.js" type="text/javascript"></script>
    </body>


</html>