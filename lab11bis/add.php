<?php
    
$servername = "localhost";
$username = "root";
$password = "";
$dbName = "myDB";
$tableName = "class";

include('functions.php');

$conn = connect($servername, $username, $password, $dbName);

function insertTable($db, $table, $fname, $lname, $mat){
    $insert = "INSERT into " . $table . " (firstname, lastname, matricular) VALUES(?,?,?)";
    $stmt = $db->prepare($insert);
    if(!$stmt->bind_param("sss",$fname, $lname, $mat))
        die("Blindage error: " . $stmt->error);
    if (!$stmt->execute()) 
        die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
}
    
    if(!empty($_GET['fname']) AND !empty($_GET['lname']) AND !empty($_GET['mat'])){
        
insertTable($conn, $tableName,htmlspecialchars($_GET['fname']),htmlspecialchars($_GET['lname']),htmlspecialchars($_GET['mat']));
        printTable($conn, $tableName);
    }
?>
