function getRequestObject() {
    if(window.XMLHttpRequest)
        return (new XMLHttpRequest());
    else if (window.ActiveXObject)
        return (new ActiveXObject("Microsoft.XMLHTTP"));
     else 
        return(null);
}


function addFunction(){
    request = getRequestObject();
    
    if(request != null){
        var fname = document.getElementById("addfname");
        var lname = document.getElementById("addlname");
        var mat = document.getElementById("addmat");
        var url = "add.php?fname=" + fname.value +"&lname=" + lname.value + "&mat=" + mat.value;
        console.log(url);
        request.open('GET', url, true);
        request.onreadystatechange =
            function(){
                if(request.readyState==4){
                    var table = document.getElementById("tableSection");
                    table.innerHTML = request.responseText;
                    fname.value="";
                    lname.value="";
                    mat.value="";
                }
            };
        request.send(null);
    }
}

function deleteFunction(){
    request = getRequestObject();
    
    if(request != null){
        var mat = document.getElementById("delmat");
        var url = "delete.php?mat=" + mat.value;
        console.log(url);
        request.open('GET', url, true);
        request.onreadystatechange =
            function(){
                if(request.readyState==4){
                    var table = document.getElementById("tableSection");
                    table.innerHTML = request.responseText;
                    mat.value="";
                }
            };
        request.send(null);
    }
}

function setFunction(){
    request = getRequestObject();
    
    if(request != null){
        var mat = document.getElementById("setmat");
        var lname = document.getElementById("setlname");
        var fname = document.getElementById("setfname");
        var url = "set.php?mat=" + mat.value + "&fname=" + fname.value + "&lname=" + lname.value ;
        console.log(url);
        request.open('GET', url, true);
        request.onreadystatechange =
            function(){
                if(request.readyState==4){
                    var table = document.getElementById("tableSection");
                    table.innerHTML = request.responseText;
                    fname.value="";
                    lname.value="";
                    mat.value="";
                }
            };
        request.send(null);
    }
}

function sendRequest(search, result){

    request=getRequestObject();

    if(request!=null){
        var userInput = document.getElementById(search);
        var url='ajax.php?search=' +search+ '&pattern='+userInput.value;
        console.log(url);
        request.open('GET',url,true);
        request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    var ajaxResponse=document.getElementById(result);
                    ajaxResponse.innerHTML=request.responseText;
                    ajaxResponse.style.visibility="visible";
                    $("#loader").attr('display', 'none');
                } else if(request.readyState<4)
                    $("#loader").attr('display', 'inline');

            };
        request.send(null);
    }
    
    changeTable(search);
}

function selectValue(search, result) {
    var list=document.getElementById("list" + search);
    var userInput=document.getElementById(search);
    var ajaxResponse=document.getElementById(result);
    userInput.value=list.options[list.selectedIndex].text;
    ajaxResponse.style.visibility="hidden";
    
}

function changeTable(search){

    xhr=getRequestObject();

    if(xhr!=null){
        var userInput = document.getElementById(search);
        var url='refreshTable.php?search=' +search+ '&pattern='+userInput.value;
        console.log(url);
        xhr.open('GET',url,true);
        xhr.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    var tab = document.getElementById("tableSection");
                    tab.innerHTML=xhr.responseText;
                    $("#loader").attr('display', 'none');
                } else if(request.readyState<4)
                    $("#loader").attr('display', 'inline');
            };
        xhr.send(null);
    }
}
