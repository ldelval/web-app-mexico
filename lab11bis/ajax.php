<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbName = "myDB";
$tableName = "class";

include('functions.php');

$columnAssoc = array('fnameSearch'=>'firstname', 'lnameSearch'=>'lastname', 'matSearch'=>'matricular');
$resultAssoc = array('fnameSearch'=>'resultFName', 'lnameSearch'=>'resultLName', 'matSearch'=>'resultMat');

$conn = connect($servername, $username, $password, $dbName);

$search=$_GET['search'];
$words=select($conn, $tableName, $columnAssoc[$search]);

$response="";
$size=0;

foreach($words as $word){
    $pos=stripos(strtolower($word),strtolower($_GET['pattern']));
    if(!($pos===false)){
        $size++;
        $response.="<option value=\"$word\">$word</option>";
    }
}


if($size>0)
    echo "<select id=\"list". $search . "\" size=$size onclick=\"
    selectValue('". $search . "', '" . $resultAssoc[$search] . "')\"
    >$response</select>";



?>
