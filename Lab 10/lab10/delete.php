<?php
    $tableName = "class";
    $dbName = "myDB";

    $conn = new mysqli("localhost", "root", "", $dbName);
    if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);

    function searchForMat($db, $table, $mat){
        $select = "SELECT id, matricular FROM " . $table;
        if ($result = $db->query($select)) {
            while ($row = $result->fetch_assoc()) {
                if($row['matricular'] == $mat)
                    return $row["id"];
            }
            $result->free();
            return 0;
        }
        else die('Selection error ' . $result->error);
    }

    function delete($db, $table, $id){
            $delete = "DELETE from " . $table . " WHERE id=" . $id;
            if (!$db->query($delete))
                die("Deleting n° : " . $id . " failed. (" . $stmt->errno . ") " . $stmt->error);
        }

    function deleteByMat($db, $table, $mat){
        $id = searchForMat($db, $table, $mat);
        delete($db, $table, $id);
    }
    
    if(isset($_POST['mat'])){
        deleteByMat($conn, $tableName, htmlspecialchars($_POST['mat']));
        include('index.php');
    }
?>
