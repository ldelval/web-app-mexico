<?php
    $tableName = "class";
    $dbName = "myDB";

    $conn = new mysqli("localhost", "root", "", $dbName);
    if ($conn->connect_error)
            die("Connection failed: " . $conn->connect_error);

    function insertTable($db, $table, $fname, $lname, $mat){
        $insert = "INSERT into " . $table . " (firstname, lastname, matricular) VALUES(?,?,?)";
        
        
        $stmt = $db->prepare($insert);
        if(!$stmt->bind_param("sss",$fname, $lname, $mat))
            die("Blindage error: " . $stmt->error);
        if (!$stmt->execute()) 
            die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
    }
    
    if(isset($_POST['fname']) AND isset($_POST['lname']) AND isset($_POST['mat'])){
        insertTable($conn, $tableName, htmlspecialchars($_POST['fname']), htmlspecialchars($_POST['lname']), htmlspecialchars($_POST['mat']));
        include('index.php');
    }
?>
