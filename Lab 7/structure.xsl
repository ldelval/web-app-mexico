<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <html>
        <head>
            <title>McDonald's</title>
        </head>
        <body>


            <table>
                <tr>
                    <th>Burger</th>
                    <th>Date of Launch</th>
                    <th>Meat</th>
                    <th>Grade</th>
                </tr>
                <xsl:for-each select="mcdonalds/burger">
                    <tr>
                        <td><xsl:value-of select="name"/></td>
                        <td><xsl:value-of select="launch"/></td>
                        <td><xsl:value-of select="meat"/></td>
                        <td><xsl:value-of select="grade"/></td>
                    </tr>
                </xsl:for-each>
            </table>
			<questions>
			<br/> <b>What are the goals of the XML technology?</b>
			<br/> The goal of XML is to store and transport data? It is focused on what data is, not how data looks and it allows to use a self-descriptive language.
			<br/> <b>What are the advantages to use XML Schemas over DTDs?</b>
			<br/> XML schemas use XML language (self-descriptive) unlike mientras que DTD have their own syntax, which is to learn.
            <br/> <b>What is your opinion about storing data in XML documents?</b>
            <br/> I am more at ease with html language but I think it is rather nice to write your own language and make it self-descriptive.	
            </questions> 			
        </body>
    </html>    
</xsl:template>

</xsl:stylesheet>